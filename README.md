What this is all about - Running whipper in a docker container
==============================================================

I like to install my software from the official debian repos (and maybe some
other debian repos I trust and prefer not to make, make install or pip install
whenever I can avoid it). I was playing around with whipper, which is a very
cool piece of software to rip cd's accurately and just met docker and thought it
would be a cool idea to run it in there. So here you go.


Installing docker
=================

To install docker, just follow the official instructions:

https://docs.docker.com/engine/installation/


Building docker image
=====================

```
cd /tmp
git clone https://gitlab.com/juanitobananas/whipper-in-a-docker.git
cd whipper-in-a-docker
docker build -t whipper-in-a-docker .
```

It is important that it is called `whipper-in-a-docker` so that the run script
will work. Of course it is also possible to change the script or to create the
docker container manually.

After you've built the docker image, you can safely delete the
whipper-in-a-docker directory.

Running the docker image (creating a container)
===============================================

Just run `run-whipper-in-a-docker.sh` script. You will end up inside a docker
container where you can rip cd's using whipper (as explained here:
https://github.com/JoeLametta/whipper).

Because the data in the docker container isn't stored anywhere permanently, the
whipper config files will be stored in your home folder (on your host machine):
`${HOME}/.whipper-in-a-docker` and the rips will be written to 
`${HOME}/whipper-rips`. Both of these values can be easily changed in the
`run-whipper-in-a-docker.sh` script.

Miscellaneous
=============

I have only tested this with Linux but I guess this should work for both windows
and mac too. You'll have to change the starting script for sure (mostly to bind
the cd/dvd-drive correctly). If you manage to do this and tell me how, I can
add the instructions to this readme.